using System.Linq;
using System.Media;
using System.Threading;
using System.Threading.Tasks;
using AudioSwitcher.AudioApi;
using AudioSwitcher.AudioApi.CoreAudio;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NAudio.CoreAudioApi;
using Role = AudioSwitcher.AudioApi.Role;

namespace MicChangerService
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly IConfiguration _configuration;
        
        private static MMDeviceEnumerator _enumer = new MMDeviceEnumerator();
        private static MMDevice _outputDevice = _enumer.GetDefaultAudioEndpoint(DataFlow.Render, NAudio.CoreAudioApi.Role.Multimedia);
        private static CoreAudioController _audioController = new CoreAudioController();
        
        private static SoundPlayer _soundPlayer;

        private static CoreAudioDevice _currentMic;
        private static CoreAudioDevice _mainMic;
        private static CoreAudioDevice _headsetMic;
        
        private static int _clicks;

        public Worker(ILogger<Worker> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _outputDevice.AudioEndpointVolume.OnVolumeNotification += VolumeChanged;
            
            _mainMic =
                _audioController.GetCaptureDevices()
                    .FirstOrDefault(x => x.FullName.Contains(_configuration.GetValue<string>("Microphones:Main")));
            _headsetMic =
                _audioController.GetCaptureDevices()
                    .FirstOrDefault(x => x.FullName.Contains(_configuration.GetValue<string>("Microphones:Headset")));
            
            _soundPlayer = new SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "change.wav");

            while (!stoppingToken.IsCancellationRequested)
            {
                _clicks = 0;
                await Task.Delay(1500, stoppingToken);
            }
        }

        void VolumeChanged(AudioVolumeNotificationData data)
        {
            _clicks++;

            if (_clicks >= 2)
            {
                _currentMic =
                    _audioController.GetDefaultDevice(DeviceType.Capture, Role.Communications);
                
                _soundPlayer.Play();

                if (_currentMic == _mainMic) _headsetMic.SetAsDefaultCommunications();
                else _mainMic.SetAsDefaultCommunications();
                
                _logger.LogInformation($"Mic changed to {_currentMic.FullName}");
            }
        }
    }
}