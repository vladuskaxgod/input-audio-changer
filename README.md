# Input Audio Changer

Kinda helpful tool to change default input audio device. Might be very helpful for users with 2 different mics (standed and wireless headset) and be able to switch them when getting up from PC. Headset must have increase/decrease volume buttons to work. 

## Usage

- [ ] Clone repo
- [ ] Restore nuget packages
```
nuget restore MicChangerService.sln
```

- [ ] Run mmsys.cpl and check your mic names in "Recordings" tab. Change namings in appsettings.json according to mmsys.cpl (first word in name is enough)
```
{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Information"
    }
  },
  "Microphones": {
	  "Main": "stand mic name",
	  "Headset": "headset mic name"
  }
}
```

- [ ] Build from sources
```
dotnet publish -o "C:\Users\Public\MicChanger\"
```

- [ ] Install service by using New-Service CLI in powershell using administrator mode
```
New-Service -Name MicChanger -BinaryPathName "C:\Users\Public\MicChanger\MicChangerService.exe" -StartupType Automatic
```

- [ ] Run service in task manager
- [ ] Test by pressing volume button twice

**WARNING**
Buttons have to change volume literally, otherwise it won't work. In my case, the best way is press volume- then volume+ 